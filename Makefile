CC=gcc
CFLAGS=-march=native -O2 -pipe -std=c99 -pedantic -Wall
LDFLAGS=

CFLAGS+=-ggdb
LDFLAGS+=-ggdb

all: acc

acc: main.o
	$(CC) $(LDFLAGS) -o $@ main.o

clean:
	rm -f -- acc *.o
