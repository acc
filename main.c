/* Alexey's C compiler. */
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdarg.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <limits.h>

struct pos {
	unsigned int line, column;
};

#ifdef __GNUC__
#define __noreturn	__attribute((noreturn))
#define __printf(a, b)	__attribute__((format(printf, a, b)))
#else
#define __noreturn
#define __printf(a, b)
#endif

static void warning(struct pos *pos, const char *fmt, ...) __printf(2, 3);
static void warning(struct pos *pos, const char *fmt, ...)
{
	va_list args;

	fprintf(stderr, "%u:%u: warning: ", pos->line, pos->column);
	va_start(args, fmt);
	vfprintf(stderr, fmt, args);
	va_end(args);
	fputc('\n', stderr);
}

static void error_exit(struct pos *pos, const char *fmt, ...) __printf(2, 3) __noreturn;
static void error_exit(struct pos *pos, const char *fmt, ...)
{
	va_list args;

	fprintf(stderr, "%u:%u: error: ", pos->line, pos->column);
	va_start(args, fmt);
	vfprintf(stderr, fmt, args);
	va_end(args);
	fputc('\n', stderr);
	exit(EXIT_FAILURE);
}

static void perror_exit(const char *fmt, ...) __printf(1, 2) __noreturn;
static void perror_exit(const char *fmt, ...)
{
	int old_errno = errno;
	va_list args;

	fputs("acc: ", stderr);
	va_start(args, fmt);
	vfprintf(stderr, fmt, args);
	va_end(args);
	fputs(": ", stderr);
	errno = old_errno;
	perror(NULL);
	exit(EXIT_FAILURE);
}

static void _error_exit(const char *fmt, ...) __printf(1, 2) __noreturn;
static void _error_exit(const char *fmt, ...)
{
	va_list args;

	fputs("acc: error: ", stderr);
	va_start(args, fmt);
	vfprintf(stderr, fmt, args);
	va_end(args);
	fputc('\n', stderr);
	exit(EXIT_FAILURE);
}

static void *xmalloc(size_t size)
{
	void *p;

	p = malloc(size);
	if (!p)
		perror_exit("%s: size %zu", __func__, size);
	return p;
}

static void *xmemdup(const void *src, size_t n)
{
	void *dst;

	dst = xmalloc(n);
	memcpy(dst, src, n);
	return dst;
}

static ssize_t _xread(int fd, void *buf, size_t count)
{
	ssize_t rv;

	do {
		rv = read(fd, buf, count);
	} while (rv == -1 && (errno == EAGAIN || errno == EINTR));
	return rv;
}

static void xread(int fd, void *buf, size_t count)
{
	while (count > 0) {
		ssize_t rv;

		rv = _xread(fd, buf, count);
		if (rv == -1)
			perror_exit("read fd %d, buf %p, count %zu", fd, buf, count);
		if (rv == 0)
			_error_exit("fd %d truncated, buf %p, count %zu", fd, buf, count);

		buf = (char *)buf + rv;
		count -= rv;
	}
}

static void fix_newline(char *c, unsigned int *nr_c)
{
	unsigned int i;

	i = 0;
	while (i < *nr_c) {
		if (c[i] == '\r' && i + 1 < *nr_c && c[i + 1] == '\n') {
			memmove(&c[i], &c[i + 1], *nr_c - i - 1);
			(*nr_c)--;
		} else if (c[i] == '\r') {
			c[i] = '\n';
			i++;
		} else
			i++;
	}
}

static struct pos *line_column(const char *c, unsigned int nr_c)
{
	struct pos *pos;
	unsigned int line, column;
	unsigned int i;

	if (nr_c >= 0xffffffff / sizeof(struct pos))
		_error_exit("integer overflow nr_c %u", nr_c);
	pos = xmalloc(nr_c * sizeof(struct pos));

	line = 1;
	column = 1;
	for (i = 0; i < nr_c; i++) {
		pos[i].line = line;
		pos[i].column = column;

		if (c[i] == '\n') {
			line++;
			column = 1;
		} else
			column++;
	}
	return pos;
}

static void warn_trigraph(const char *c, unsigned int nr_c, struct pos *pos)
{
	unsigned int i;

	i = 0;
	while (i + 2 < nr_c) {
		if (c[i] == '?' && c[i + 1] == '?') {
			switch (c[i + 2]) {
			case '=':case ')':case '!':
			case '(':case '\'':case '>':
			case '/':case '<':case '-':
				warning(&pos[i], "trigraph sequence ??%c, ignoring", (unsigned char)c[i + 2]);
				i += 3;
				break;
			default:
				i++;
			}
		} else
			i++;
	}
}

static void delete_backslash_newline(char *c, unsigned int *nr_c, struct pos *pos)
{
	unsigned int i;

	i = 0;
	while (i + 1 < *nr_c) {
		if (c[i] == '\\' && c[i + 1] == '\n') {
			unsigned int nr_to_move = *nr_c - i - 2;

			memmove(&c[i], &c[i + 2], nr_to_move);
			memmove(&pos[i], &pos[i + 2], nr_to_move * sizeof(struct pos));
			(*nr_c) -= 2;
		} else
			i++;
	}
}

struct pp_token {
	struct pp_token *next;
	enum pp_token_type {
		PP_TOKEN_IDENTIFIER = UCHAR_MAX + 1,
		PP_TOKEN_NUMBER,
		PP_TOKEN_STRING,
		PP_TOKEN_CHAR,
		PP_TOKEN_INCLUDE_GLOBAL,	/* #include < */
		PP_TOKEN_INCLUDE_LOCAL,		/* #include " */
		PP_TOKEN_DEFINE,		/* #define */
		PP_TOKEN_IFDEF,			/* #ifdef */
		PP_TOKEN_IFNDEF,		/* #ifndef */

#define _2(c1, c2)	(((c1) << 8) | (c2))
#define _3(c1, c2, c3)	(((c1) << 16)| ((c2) << 8) | (c3))
		PP_TOKEN_DOTDOTDOT	= _3('.', '.', '.'),
		PP_TOKEN_DEREFERENCE	= _2('-', '>'),
		PP_TOKEN_SUB_EQ		= _2('-', '='),
		PP_TOKEN_DEC		= _2('-', '-'),
		PP_TOKEN_ADD_EQ		= _2('+', '='),
		PP_TOKEN_INC		= _2('+', '+'),
		PP_TOKEN_AND_EQ		= _2('&', '='),
		PP_TOKEN_AND		= _2('&', '&'),
		PP_TOKEN_MUL_EQ		= _2('*', '='),
		PP_TOKEN_NOT_EQ		= _2('!', '='),
		PP_TOKEN_DIV_EQ		= _2('/', '='),
		PP_TOKEN_REM_EQ		= _2('%', '='),
		PP_TOKEN_LSHIFT_EQ	= _3('<', '<', '='),
		PP_TOKEN_LSHIFT		= _2('<', '<'),
		PP_TOKEN_LEQ		= _2('<', '='),
		PP_TOKEN_RSHIFT_EQ	= _3('>', '>', '='),
		PP_TOKEN_RSHIFT		= _2('>', '>'),
		PP_TOKEN_GEQ		= _2('>', '='),
		PP_TOKEN_EQ		= _2('=', '='),
		PP_TOKEN_XOR_EQ		= _2('^', '='),
		PP_TOKEN_OR_EQ		= _2('|', '='),
		PP_TOKEN_OR		= _2('|', '|'),
		PP_TOKEN_SHARPSHARP	= _2('#', '#'),
#undef _2
#undef _3
	} type;
	char *str;	/* string representation, if type is not enough */
	struct pos pos;
};

static struct pp_token *pp_token_create(enum pp_token_type type, struct pos *pos)
{
	struct pp_token *ppt;

	ppt = xmalloc(sizeof(struct pp_token));
	ppt->next = NULL;
	ppt->type = type;
	ppt->str = NULL;
	ppt->pos = *pos;
	return ppt;
}

/* [start, end) */
static void pp_token_add(struct pp_token *ppt, const char *c, unsigned int start, unsigned int end)
{
	ppt->str = xmemdup(&c[start], end - start + 1);
	ppt->str[end - start] = '\0';
}

static void pp_token_free(struct pp_token *ppt_head)
{
	struct pp_token *ppt;

	ppt = ppt_head;
	while (ppt) {
		struct pp_token *next;

		next = ppt->next;
		if (ppt->str)
			free(ppt->str);
		free(ppt);
		ppt = next;
	}
}

static void pp_token_print(struct pp_token *ppt)
{
	printf("%u:%u:\t", ppt->pos.line, ppt->pos.column);
	switch (ppt->type) {
	case '\n':
		printf("\\n");
		break;
	case ' ':
		printf("' '");
		break;
	case PP_TOKEN_IDENTIFIER:
		printf("pp-identifier %s", ppt->str);
		break;
	case PP_TOKEN_NUMBER:
		printf("pp-number %s", ppt->str);
		break;
	case PP_TOKEN_STRING:
		printf("pp-string \"%s\"", ppt->str);
		break;
	case PP_TOKEN_CHAR:
		printf("pp-char '%s'", ppt->str);
		break;
	case PP_TOKEN_INCLUDE_GLOBAL:
		printf("#include <%s>", ppt->str);
		break;
	case PP_TOKEN_INCLUDE_LOCAL:
		printf("#include \"%s\"", ppt->str);
		break;
	case PP_TOKEN_DEFINE:
		printf("#define %s", ppt->str);
		break;
	case PP_TOKEN_IFDEF:
		printf("#ifdef %s", ppt->str);
		break;
	case PP_TOKEN_IFNDEF:
		printf("#ifndef %s", ppt->str);
		break;
	case PP_TOKEN_DOTDOTDOT:
	case PP_TOKEN_LSHIFT_EQ:
	case PP_TOKEN_RSHIFT_EQ:
		printf("%c%c%c", (ppt->type >> 16) & 0xff, (ppt->type >> 8) & 0xff, ppt->type & 0xff);
		break;
	case PP_TOKEN_DEREFERENCE:
	case PP_TOKEN_SUB_EQ:
	case PP_TOKEN_DEC:
	case PP_TOKEN_ADD_EQ:
	case PP_TOKEN_INC:
	case PP_TOKEN_AND_EQ:
	case PP_TOKEN_AND:
	case PP_TOKEN_MUL_EQ:
	case PP_TOKEN_NOT_EQ:
	case PP_TOKEN_DIV_EQ:
	case PP_TOKEN_REM_EQ:
	case PP_TOKEN_LSHIFT:
	case PP_TOKEN_LEQ:
	case PP_TOKEN_RSHIFT:
	case PP_TOKEN_GEQ:
	case PP_TOKEN_EQ:
	case PP_TOKEN_XOR_EQ:
	case PP_TOKEN_OR_EQ:
	case PP_TOKEN_OR:
	case PP_TOKEN_SHARPSHARP:
		printf("%c%c", (ppt->type >> 8) & 0xff, ppt->type & 0xff);
		break;
	default:
		printf("%c", ppt->type);
	}
	putc('\n', stdout);
}

static int pp_nondigit(const char c)
{
	return ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z') || c == '_';
}

static int pp_octdigit(const char c)
{
	return '0' <= c && c <= '7';
}

static int pp_digit(const char c)
{
	return '0' <= c && c <= '9';
}

static int pp_hexdigit(const char c)
{
	return pp_digit(c) || ('a' <= c && c <= 'f') || ('A' <= c && c <= 'F');
}

static unsigned int _pp_identifier_end(const char *c, unsigned int nr_c, unsigned int start)
{
	unsigned int i;

	i = start;
	while (i < nr_c) {
		if (pp_nondigit(c[i]) || pp_digit(c[i])) {
			i++;
		} else if (i + 5 < nr_c && c[i] == '\\' && c[i + 1] == 'u' &&
			   pp_hexdigit(c[i + 2]) && pp_hexdigit(c[i + 3]) && pp_hexdigit(c[i + 4]) && pp_hexdigit(c[i + 5])) {
			i += 2 + 4;
		} else if (i + 9 < nr_c && c[i] == '\\' && c[i + 1] == 'U' &&
			   pp_hexdigit(c[i + 2]) && pp_hexdigit(c[i + 3]) && pp_hexdigit(c[i + 4]) && pp_hexdigit(c[i + 5]) &&
			   pp_hexdigit(c[i + 6]) && pp_hexdigit(c[i + 7]) && pp_hexdigit(c[i + 8]) && pp_hexdigit(c[i + 9])) {
			i += 2 + 4 + 4;
		} else
			return i;
	}
	return i;
}

static unsigned int pp_number_end(const char *c, unsigned int nr_c, unsigned int start)
{
	unsigned int i;

	i = start + 1;
	while (i < nr_c) {
		if ((c[i] == 'e' || c[i] == 'E' || c[i] == 'p' || c[i] == 'P') &&
		    i + 1 < nr_c && (c[i + 1] == '+' || c[i + 1] == '-')) {
			i += 2;
		} else if (pp_digit(c[i]) || pp_nondigit(c[i]) || c[i] == '.') {
			i++;
		} else if (c[i] == '\\' && i + 5 < nr_c && c[i + 1] == 'u' &&
			   pp_hexdigit(c[i + 2]) && pp_hexdigit(c[i + 3]) && pp_hexdigit(c[i + 4]) && pp_hexdigit(c[i + 5])) {
			i += 2 + 4;
		} else if (c[i] == '\\' && i + 9 < nr_c && c[i + 1] == 'U' &&
			   pp_hexdigit(c[i + 2]) && pp_hexdigit(c[i + 3]) && pp_hexdigit(c[i + 4]) && pp_hexdigit(c[i + 5]) &&
			   pp_hexdigit(c[i + 6]) && pp_hexdigit(c[i + 7]) && pp_hexdigit(c[i + 8]) && pp_hexdigit(c[i + 9])) {
			i += 2 + 4 + 4;
		} else
			return i;
	}
	return i;
}

static unsigned int c_comment_end(const char *c, unsigned int nr_c, unsigned int start)
{
	unsigned int i;

	i = start + 2;
	while (i + 1 < nr_c) {
		if (c[i] == '*' && c[i + 1] == '/')
			return i + 2;
		i++;
	}
	return nr_c;
}

static unsigned int cpp_comment_end(const char *c, unsigned int nr_c, unsigned int start)
{
	unsigned int i;

	i = start + 2;
	while (i < nr_c && c[i] != '\n')
		i++;
	return i;
}

static unsigned int escape_sequence_end(const char *c, unsigned int nr_c, unsigned int start, struct pos *_pos)
{
	struct pos *pos = &_pos[start];
	unsigned int i;

	i = start + 1;
	if (i >= nr_c)
		error_exit(pos, "incomplete escape sequence");
	switch (c[i]) {
	case '\'':case '"':case '?':case '\\':
	case 'a':case 'b':case 'f':case 'n':case 'r':case 't':case 'v':
		return i + 1;
	case '0':case '1':case '2':case '3':case '4':case '6':case '7':
		if (i + 2 < nr_c && pp_octdigit(c[i + 1]) && pp_octdigit(c[i + 2]))
			return i + 3;
		if (i + 1 < nr_c && pp_octdigit(c[i + 1]))
			return i + 2;
		return i + 1;
	case 'x':
		i++;
		while (i < nr_c && pp_hexdigit(c[i]))
			i++;
		if (i == start + 2)
			error_exit(pos, "invalid hexadecimal escape sequence");
		return i;
	case 'u':
		if (i + 4 < nr_c &&
		    pp_hexdigit(c[i + 1]) && pp_hexdigit(c[i + 2]) && pp_hexdigit(c[i + 3]) && pp_hexdigit(c[i + 4]))
			return i + 5;
		error_exit(pos, "invalid universal character name");
	case 'U':
		if (i + 8 < nr_c &&
		    pp_hexdigit(c[i + 1]) && pp_hexdigit(c[i + 2]) && pp_hexdigit(c[i + 3]) && pp_hexdigit(c[i + 4]) &&
		    pp_hexdigit(c[i + 5]) && pp_hexdigit(c[i + 6]) && pp_hexdigit(c[i + 7]) && pp_hexdigit(c[i + 8]))
			return i + 9;
		error_exit(pos, "invalid universal character name");
	default:
		error_exit(pos, "invalid escape sequence");
	}
}

static unsigned int pp_string_end(const char *c, unsigned int nr_c, unsigned int start, struct pos *_pos)
{
	struct pos *pos = &_pos[start];
	unsigned int i;

	i = start + 1;
	while (i < nr_c && c[i] != '"') {
		switch (c[i]) {
		case '\n':
			goto incomplete;
		case '\\':
			i = escape_sequence_end(c, nr_c, i, _pos);
			break;
		default:
			i++;
		}
	}
	if (i >= nr_c)
		goto incomplete;
	return i + 1;

incomplete:
	error_exit(pos, "incomplete string literal");
}

static unsigned int pp_char_end(const char *c, unsigned int nr_c, unsigned int start, struct pos *_pos)
{
	struct pos *pos = &_pos[start];
	unsigned int i;

	i = start + 1;
	while (i < nr_c && c[i] != '\'') {
		switch (c[i]) {
		case '\n':
			goto incomplete;
		case '\\':
			i = escape_sequence_end(c, nr_c, i, _pos);
			break;
		default:
			i++;
		}
	}
	if (i >= nr_c)
		goto incomplete;
	if (i == start + 1)
		goto empty;
	return i + 1;

incomplete:
	error_exit(pos, "incomplete character constant");
empty:
	error_exit(pos, "empty character constant");
}

static unsigned int whitespace_end(const char *c, unsigned int nr_c, unsigned int start)
{
	unsigned int i;

	i = start;
	while (i < nr_c && (c[i] == ' ' || c[i] == '\t'))
		i++;
	return i;
}

static struct pp_token *pp_define(const char *c, unsigned int nr_c, unsigned int *start, struct pos *pos)
{
	struct pp_token *ppt;
	unsigned int name_start, name_end;
	unsigned int i;

	i = *start;
	if (i >= nr_c || !pp_nondigit(c[i]))
		error_exit(pos, "invalid #define directive");
	name_start = i;
	i = _pp_identifier_end(c, nr_c, i + 1);
	name_end = i;

	ppt = pp_token_create(PP_TOKEN_DEFINE, pos);
	pp_token_add(ppt, c, name_start, name_end);

	while (i < nr_c && c[i] != '\n')
		i++;
	*start = i + 1;
	return ppt;
}

static struct pp_token *pp_error(const char *c, unsigned int nr_c, unsigned int *start, struct pos *pos)
{
	unsigned int j;

	j = *start;
	while (j < nr_c && c[j] != '\n')
		j++;
	error_exit(pos, "%.*s", j - *start, &c[*start]);
}

static struct pp_token *pp_include(const char *c, unsigned int nr_c, unsigned int *start, struct pos *pos)
{
	struct pp_token *ppt;
	unsigned int name_start, name_end;
	enum pp_token_type type;
	unsigned int j;

	j = *start;
	if (j + 2 >= nr_c || (c[j] != '<' && c[j] != '"'))
		error_exit(pos, "invalid #include directive");
	j++;
	name_start = j;
	if (c[j - 1] == '<') {
		while (j < nr_c && c[j] != '>')
			j++;
		type = PP_TOKEN_INCLUDE_GLOBAL;
	} else {
		while (j < nr_c && c[j] != '"')
			j++;
		type = PP_TOKEN_INCLUDE_LOCAL;
	}
	if (j >= nr_c)
		error_exit(pos, "invalid #include directive");
	name_end = j;

	ppt = pp_token_create(type, pos);
	pp_token_add(ppt, c, name_start, name_end);

	j++;
	if (j < nr_c && c[j] == '\n')
		j++;
	*start = j;
	return ppt;
}

static struct pp_token *pp_ifdef(const char *c, unsigned int nr_c, unsigned int *start, struct pos *pos)
{
	struct pp_token *ppt;
	unsigned int name_start, name_end;
	unsigned int i;

	i = *start;
	if (i >= nr_c || !pp_nondigit(c[i]))
		error_exit(pos, "invalid #ifdef directive");
	name_start = i;
	i = _pp_identifier_end(c, nr_c, i + 1);
	name_end = i;

	ppt = pp_token_create(PP_TOKEN_IFDEF, pos);
	pp_token_add(ppt, c, name_start, name_end);

	while (i < nr_c && c[i] != '\n')
		i++;
	*start = i + 1;
	return ppt;
}

static struct pp_token *pp_ifndef(const char *c, unsigned int nr_c, unsigned int *start, struct pos *pos)
{
	struct pp_token *ppt;
	unsigned int name_start, name_end;
	unsigned int i;

	i = *start;
	if (i >= nr_c || !pp_nondigit(c[i]))
		error_exit(pos, "invalid #ifndef directive");
	name_start = i;
	i = _pp_identifier_end(c, nr_c, i + 1);
	name_end = i;

	ppt = pp_token_create(PP_TOKEN_IFNDEF, pos);
	pp_token_add(ppt, c, name_start, name_end);

	while (i < nr_c && c[i] != '\n')
		i++;
	*start = i + 1;
	return ppt;
}

static struct pp_token *pp_tokenize(const char *c, unsigned int nr_c, struct pos *_pos)
{
	struct pp_token *ppt_head, *ppt_tail;
	int pp_directive_allowed;
	unsigned int i;

	ppt_head = NULL;
	pp_directive_allowed = 1;
	i = 0;
	while (i < nr_c) {
		struct pos *pos;
		struct pp_token *ppt;

		if (pp_directive_allowed) {
			static const struct __ppd {
				unsigned int len;
				const char *str;
				struct pp_token * (*f)(const char *c, unsigned int nr_c, unsigned int *start, struct pos *pos);
			} _ppd[] = {
				{ .len = 6,	.str = "define",	.f = pp_define,		},
				{ .len = 5,	.str = "error",		.f = pp_error,		},
				{ .len = 7,	.str = "include",	.f = pp_include,	},
				{ .len = 5,	.str = "ifdef",		.f = pp_ifdef,		},
				{ .len = 6,	.str = "ifndef",	.f = pp_ifndef,		},
			};
			unsigned int sharp_start, ppd_start, ppd_end;
			unsigned int j, k;

			j = whitespace_end(c, nr_c, i);
			if (j >= nr_c || c[j] != '#')
				goto not_pp_directive;
			sharp_start = j;
			pos = &_pos[sharp_start];
			j = whitespace_end(c, nr_c, j + 1);
			if (j >= nr_c) {
				warning(pos, "empty preprocessor directive");
				i = j;
				continue;
			}
			if (c[j] == '\n') {
				warning(pos, "empty preprocessor directive");
				/* Eat newline after # */
				i = j + 1;
				continue;
			}

			ppd_start = j;
			while (j < nr_c && pp_nondigit(c[j]))
				j++;
			ppd_end = j;

			for (k = 0; k < sizeof(_ppd) / sizeof(_ppd[0]); k++) {
				const struct __ppd *ppd = &_ppd[k];

				if (ppd_end - ppd_start == ppd->len && memcmp(&c[ppd_start], ppd->str, ppd->len) == 0)
					break;
			}
			if (k >= sizeof(_ppd) / sizeof(_ppd[0]))
				error_exit(pos, "unknown preprocessor directive '#%.*s'", ppd_end - ppd_start, &c[ppd_start]);
			j = whitespace_end(c, nr_c, j);
			ppt = _ppd[k].f(c, nr_c, &j, pos);
			i = j;
			goto pp_token_link;
		}

not_pp_directive:
		pos = &_pos[i];
		switch (c[i]) {
			unsigned int j;

		case '\t':
		case ' ':
			ppt = pp_token_create(' ', pos);
			i++;
			break;
		case '\n':
			ppt = pp_token_create('\n', pos);
			i++;
			break;
		case '[':case ']':
		case '(':case ')':
		case '{':case '}':
		case '~':
		case '?':
		case ':':
		case ';':
		case ',':
pp_token_simple:
			ppt = pp_token_create(c[i], pos);
			i++;
			break;
		case 'a':case 'b':case 'c':case 'd':case 'e':case 'f':case 'g':
		case 'h':case 'i':case 'j':case 'k':case 'l':case 'm':case 'n':
		case 'o':case 'p':case 'q':case 'r':case 's':case 't':case 'u':
		case 'v':case 'w':case 'x':case 'y':case 'z':
		case 'A':case 'B':case 'C':case 'D':case 'E':case 'F':case 'G':
		case 'H':case 'I':case 'J':case 'K':case 'L':case 'M':case 'N':
		case 'O':case 'P':case 'Q':case 'R':case 'S':case 'T':case 'U':
		case 'V':case 'W':case 'X':case 'Y':case 'Z':
		case '_':
			ppt = pp_token_create(PP_TOKEN_IDENTIFIER, pos);
			j = _pp_identifier_end(c, nr_c, i + 1);
			pp_token_add(ppt, c, i, j);
			i = j;
			break;
		case '\\':
			if (i + 5 < nr_c && c[i + 1] == 'u' &&
			    pp_hexdigit(c[i + 2]) && pp_hexdigit(c[i + 3]) && pp_hexdigit(c[i + 4]) && pp_hexdigit(c[i + 5])) {
				ppt = pp_token_create(PP_TOKEN_IDENTIFIER, pos);
				j = _pp_identifier_end(c, nr_c, i + 2 + 4);
				pp_token_add(ppt, c, i, j);
				i = j;
			} else if (i + 9 < nr_c && c[i + 1] == 'U' &&
				   pp_hexdigit(c[i + 2]) && pp_hexdigit(c[i + 3]) && pp_hexdigit(c[i + 4]) && pp_hexdigit(c[i + 5]) &&
				   pp_hexdigit(c[i + 6]) && pp_hexdigit(c[i + 7]) && pp_hexdigit(c[i + 8]) && pp_hexdigit(c[i + 9])) {
				ppt = pp_token_create(PP_TOKEN_IDENTIFIER, pos);
				j = _pp_identifier_end(c, nr_c, i + 2 + 4 + 4);
				pp_token_add(ppt, c, i, j);
				i = j;
			} else
				error_exit(pos, "unknown character %08"PRIx32, c[i]);
			break;
		case '0':case '1':case '2':case '3':case '4':
		case '5':case '6':case '7':case '8':case '9':
			ppt = pp_token_create(PP_TOKEN_NUMBER, pos);
			j = pp_number_end(c, nr_c, i);
			pp_token_add(ppt, c, i, j);
			i = j;
			break;
		case '.':
			if (i + 2 < nr_c && c[i + 1] == '.' && c[i + 2] == '.') {
				ppt = pp_token_create(PP_TOKEN_DOTDOTDOT, pos);
				i += 3;
			} else if (i + 1 < nr_c && pp_digit(c[i + 1])) {
				ppt = pp_token_create(PP_TOKEN_NUMBER, pos);
				j = pp_number_end(c, nr_c, i + 1);
				pp_token_add(ppt, c, i, j);
				i = j;
			} else
				goto pp_token_simple;
			break;
		case '"':
			ppt = pp_token_create(PP_TOKEN_STRING, pos);
			j = pp_string_end(c, nr_c, i, _pos);
			pp_token_add(ppt, c, i + 1, j - 1);
			i = j;
			break;
		case '\'':
			ppt = pp_token_create(PP_TOKEN_CHAR, pos);
			j = pp_char_end(c, nr_c, i, _pos);
			pp_token_add(ppt, c, i + 1, j - 1);
			i = j;
			break;
		case '/':
			if (i + 1 < nr_c && c[i + 1] == '*') {
				ppt = pp_token_create(' ', pos);
				i = c_comment_end(c, nr_c, i);
			} else if (i + 1 < nr_c && c[i + 1] == '/') {
				warning(pos, "C++ comment");
				ppt = pp_token_create(' ', pos);
				i = cpp_comment_end(c, nr_c, i);
			} else if (i + 1 < nr_c && c[i + 1] == '=') {
				ppt = pp_token_create((c[i] << 8) | c[i + 1], pos);
				i += 2;
			} else
				goto pp_token_simple;
			break;
		case '-':
			if (i + 1 < nr_c && (c[i + 1] == '>' || c[i + 1] == '=' || c[i + 1] == '-')) {
				ppt = pp_token_create((c[i] << 8) | c[i + 1], pos);
				i += 2;
			} else
				goto pp_token_simple;
			break;
		case '+':
		case '&':
		case '|':
			if (i + 1 < nr_c && (c[i + 1] == '=' || c[i + 1] == c[i])) {
				ppt = pp_token_create((c[i] << 8) | c[i + 1], pos);
				i += 2;
			} else
				goto pp_token_simple;
			break;
		case '*':
		case '!':
		case '%':
		case '=':
		case '^':
			if (i + 1 < nr_c && c[i + 1] == '=') {
				ppt = pp_token_create((c[i] << 8) | c[i + 1], pos);
				i += 2;
			} else
				goto pp_token_simple;
			break;
		case '<':
		case '>':
			if (i + 2 < nr_c && c[i + 1] == c[i] && c[i + 2] == '=') {
				ppt = pp_token_create((c[i] << 16) | (c[i + 1] << 8) | c[i + 2], pos);
				i += 3;
			} else if (i + 1 < nr_c && (c[i + 1] == c[i] || c[i + 1] == '=')) {
				ppt = pp_token_create((c[i] << 8) | c[i + 1], pos);
				i += 2;
			} else
				goto pp_token_simple;
			break;
		case '#':
			if (i + 1 < nr_c && c[i + 1] == '#') {
				ppt = pp_token_create((c[i] << 8) | c[i + 1], pos);
				i += 2;
			} else
				goto pp_token_simple;
			break;
		default:
			error_exit(pos, "unknown character %08"PRIx32, c[i]);
		}

		if (ppt->type == '\n')
			pp_directive_allowed = 1;
		else if (ppt->type == ' ')
			;
		else
			pp_directive_allowed = 0;

pp_token_link:
		if (!ppt_head)
			ppt_head = ppt;
		else
			ppt_tail->next = ppt;
		ppt_tail = ppt;
	}
	return ppt_head;
}

int main(int argc, char *argv[])
{
	int fd;
	struct stat st;
	unsigned int st_size;
	void *buf;
	char *c;
	unsigned int nr_c;
	struct pos *pos;
	struct pp_token *ppt_head;

	if (argc < 2)
		return EXIT_FAILURE;

	fd = open(argv[1], O_RDONLY);
	if (fd == -1)
		perror_exit("open %s", argv[1]);
	if (fstat(fd, &st) == -1)
		perror_exit("fstat %s", argv[1]);
	if (st.st_size < 0)
		_error_exit("%s: negative st_size %"PRIdMAX, argv[1], (intmax_t)st.st_size);
	st_size = (unsigned int)(uintmax_t)(intmax_t)st.st_size;
	if ((uintmax_t)(intmax_t)st.st_size != (uintmax_t)st_size)
		_error_exit("%s: too big st_size %"PRIdMAX, argv[1], (intmax_t)st.st_size);

	buf = xmalloc(st_size);
	xread(fd, buf, st_size);
	close(fd);

	c = buf;
	nr_c = st_size;

	fix_newline(c, &nr_c);
	pos = line_column(c, nr_c);
	warn_trigraph(c, nr_c, pos);
	delete_backslash_newline(c, &nr_c, pos);

	ppt_head = pp_tokenize(c, nr_c, pos);
	free(c);
	free(pos);

	{
		struct pp_token *ppt;

		for (ppt = ppt_head; ppt; ppt = ppt->next) {
			pp_token_print(ppt);
		}
	}

	pp_token_free(ppt_head);

	return EXIT_SUCCESS;
}
